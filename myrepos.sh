#!/usr/bin/env bash

#    Copyright (C) 2024 Alex Thomae <gpl@alex.thom.ae>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


set -eEuo pipefail

# These all will be overwritten by myrepos.sh.conf below
GITLAB_COM_TOKEN=""
GITLAB_CUSTOM_TOKEN=""
GITHUB_COM_TOKEN=""
GITHUB_COM_USER=""
REPO_DIR="${HOME}/repos"

#shellcheck disable=SC1091
. "${HOME}/.config/myrepos.sh.conf"||(echo "Cannot find ${HOME}/.config/myrepos.sh.conf. Exiting"; exit 1)

function get_gitlab_data(){
    local INSTANCE CURL_OPTS URL_PART TOKEN PORT REPO REPOS
    INSTANCE="${1}"
    TOKEN="${2}"
    CURL_OPTS=("-s")
    PORT="22"
    URL_PART="api/v4/projects?simple=yes&archived=no&per_page=4000&starred=true"

    PAGES=$(curl -i "${CURL_OPTS[@]}" --header "PRIVATE-TOKEN: ${TOKEN}" "https://${INSTANCE}/${URL_PART}"|grep -E "X-Total-Pages|x-total-pages"|awk -F": " '{print $2}')
    PAGES="${PAGES//[$'\t\r\n ']}" # strip tabs, newlines, spaces
    re='^[0-9]+$'
    if [[ ! "${PAGES//}" =~ $re ]]; then
        PAGES=1
    fi
    for i in $(seq 1 "${PAGES}"); do
      echo "Retrieving page ${i}/${PAGES} of repositories from ${INSTANCE}"
      REPOS=$(curl "${CURL_OPTS[@]}" --header "PRIVATE-TOKEN: ${TOKEN}" "https://${INSTANCE}/${URL_PART}&page=${i}"|jq -r '.[].path_with_namespace')

      for REPO in ${REPOS}; do
          mr config "${REPO_DIR}/${INSTANCE}/${REPO}" checkout="git clone ssh://git@${INSTANCE}:${PORT}/${REPO}.git"
      done
    done
}

function get_github_data(){
    local CURL_OPTS USER TOKEN PORT re REPOS PAGES REPO
    INSTANCE="github.com"
    USER="${1}"
    TOKEN="${2}"
    PORT="22"
    CURL_OPTS=(
        -s
        -H 'Accept: application/vnd.github+json'
        -H 'X-GitHub-Api-Version: 2022-11-28'
        -u "${GITHUB_COM_USER}:${GITHUB_COM_TOKEN}"
    )
    URL_PART="user/starred"
    PAGES="$(curl -D - -o /dev/null "${CURL_OPTS[@]}" "https://api.${INSTANCE}/${URL_PART}?per_page=100")"
    if echo "${PAGES}"|grep link; then
        PAGES="$(echo "${PAGES}"|sed -E 's/^link.*next.*page=([0-9]+)(>; rel="last")/\1/g')"
        PAGES=${PAGES%$'\r'}
        re='^[0-9]+$'
        if [[ ! "${PAGES}" =~ $re ]]; then
            PAGES=1
        fi
    else
        PAGES=1
    fi
    for i in $(seq 1 "${PAGES}"); do
        echo "Retrieving page ${i}/${PAGES} of repositories from ${INSTANCE}"
        REPOS=$(curl "${CURL_OPTS[@]}" "https://api.${INSTANCE}/${URL_PART}?per_page=100&page=${i}"|jq -r '.[].full_name')
        for REPO in ${REPOS}; do
            mr config "${REPO_DIR}/${INSTANCE}/${REPO}" checkout="git clone ssh://git@${INSTANCE}:${PORT}/${REPO}.git"
        done
    done
}


function display_help(){
    cat << EOF

Usage: $0 (all | custom | public | config)

$(basename "${0}") - a script to generate ~/.mrconfig based on gitlab.com, github.com and any additional gitlab provider.

all       The script will generate the configuration file for myrepos with data about any repositories from any
          configured providers (GitHub, GitLab, Custom GitLab). After, it will run 'mr checkout' and 'mr upall'.
config    Will only generate the configs, but will not run 'mr checkout' nor 'mr upall'.
public    Regenerates the configuration file with the repositories hosted on gitlab.com and github.com.
custom    Regenerates the configuration file with the repositories hosted on the custom gitlab provider.

Config file:             ${HOME}/.config/myrepos.sh.conf
Include file:            ${HOME}/.config/myrepos.sh.include
Repositories directory:  ${REPO_DIR}

The contents of ~/.config.myrepos.sh.include will be included in ~/.mrconfig.

If any of the tokens is missing, the provider will be skipped. If 'REPO_DIR' is not set, "${HOME}/repos" is used.

In order for a repository to be added to the config, the current user must **star** the repository on the provider website.

Copyright (C) 2024 Alex Thomae <gpl@alex.thom.ae>
This program comes with ABSOLUTELY NO WARRANTY; for details see LICENSE.

EOF
exit 1
}

function check_deps(){
    which jq &>/dev/null || (echo "ERROR! 'jq' not found"; display_help)
    which curl &>/dev/null || (echo "ERROR! 'curl' not found"; display_help)
    which mr &>/dev/null || (echo "ERROR! 'mr' not found"; display_help)
    if [[ "$(uname)" == "Darwin" ]]; then
        which greadlink &>/dev/null || (echo "ERROR! On MacOS 'greadlink' is mandatory!"; display_help)
    fi
}

function do_backup() {
    [ -f ~/.mrconfig ] && (echo "Backing up old .mrconfing to ~/.mrconfig.bak" && mv ~/.mrconfig ~/.mrconfig.bak)
    cat <<EOF> ~/.mrconfig
[DEFAULT]
upall = git remote update -p && git merge --ff-only @{u} && git submodule init && git submodule update --recursive --remote
prune = git gc --aggressive --prune --auto
nevermind = git reset --hard HEAD && git clean -d -f
gc = git gc
EOF

    if [ -f ~/.config/myrepos.sh.include ]; then
        cat <<EOF>> ~/.mrconfig
# ~/.config/myrepos.sh.include
$(cat ~/.config/myrepos.sh.include)
EOF
    fi

}

check_deps

if [[ "$(uname)" == "Darwin" ]]; then
    REPO_DIR=$(greadlink -f "${REPO_DIR}")
else
    REPO_DIR=$(readlink -f "${REPO_DIR}")
fi

case "${1:-help}" in
    "all")
        do_backup
        if [ -n "${GITLAB_CUSTOM_TOKEN}" ] &&  [ -n "${GITLAB_CUSTOM_FQDN}" ]  ; then
            get_gitlab_data "${GITLAB_CUSTOM_FQDN}" "${GITLAB_CUSTOM_TOKEN}"
        fi
        if [ -n "${GITHUB_COM_TOKEN}" ] && [ -n "${GITHUB_COM_USER}" ]; then
            get_github_data "${GITHUB_COM_USER}" "${GITHUB_COM_TOKEN}"
        fi
        if [ -n "${GITLAB_COM_TOKEN}" ]; then
            get_gitlab_data "gitlab.com" "${GITLAB_COM_TOKEN}"
        fi
        mr checkout
        mr upall
    ;;
    "custom")
        do_backup
        if [ -n "${GITLAB_CUSTOM_TOKEN}" ] &&  [ -n "${GITLAB_CUSTOM_FQDN}" ]  ; then
            get_gitlab_data "${GITLAB_CUSTOM_FQDN}" "${GITLAB_CUSTOM_TOKEN}"
        fi
    ;;
    "public")
        do_backup
        if [ -n "${GITHUB_COM_TOKEN}" ] && [ -n "${GITHUB_COM_USER}" ]; then
            get_github_data "${GITHUB_COM_USER}" "${GITHUB_COM_TOKEN}"
        fi
        if [ -n "${GITLAB_COM_TOKEN}" ]; then
            get_gitlab_data "gitlab.com" "${GITLAB_COM_TOKEN}"
        fi
        ;;
    "config")
        do_backup
        if [ -n "${GITLAB_CUSTOM_TOKEN}" ] &&  [ -n "${GITLAB_CUSTOM_FQDN}" ]  ; then
            get_gitlab_data "${GITLAB_CUSTOM_FQDN}" "${GITLAB_CUSTOM_TOKEN}"
        fi
        if [ -n "${GITHUB_COM_TOKEN}" ] && [ -n "${GITHUB_COM_USER}" ]; then
            get_github_data "${GITHUB_COM_USER}" "${GITHUB_COM_TOKEN}"
        fi
        if [ -n "${GITLAB_COM_TOKEN}" ]; then
            get_gitlab_data "gitlab.com" "${GITLAB_COM_TOKEN}"
        fi
        ;;
    *)
        display_help
        ;;
esac

