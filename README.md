# myrepos

## Usage

```sh
$ ./myrepos.sh help

Usage: ./myrepos.sh (all | custom | public | config)

myrepos.sh - a script to generate ~/.mrconfig based on gitlab.com, github.com and any additional gitlab provider.

all       The script will generate the configuration file for myrepos with data about any repositories from any
          configured providers (GitHub, GitLab, Custom GitLab). After, it will run 'mr checkout' and 'mr upall'.
config    Will only generate the configs, but will not run 'mr checkout' nor 'mr upall'.
public    Regenerates the configuration file with the repositories hosted on gitlab.com and github.com.
custom    Regenerates the configuration file with the repositories hosted on the custom gitlab provider.

Config file:             /home/user/.config/myrepos.sh.conf
Include file:            /home/user/.config/myrepos.sh.include
Repositories directory:  /home/user/repos

The contents of ~/.config.myrepos.sh.include will be included in ~/.mrconfig.

If any of the tokens is missing, the provider will be skipped. If 'REPO_DIR' is not set, "/home/user/repos" is used.

In order for a repository to be added to the config, the current user must **star** the repository on the provider website.

```

**IMPORTANT!**
> Make sure to have [myrepos](https://myrepos.branchable.com/) installed.
> Other dependecies: `jq`, `curl`, `git`. On MacOS, make sure to have `greadlink` installed.

